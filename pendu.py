import random
from faker import Faker
import turtle

fake = Faker()

def faker_liste(nom_fichier) : #Fonction utilisant Faker pour créer un fichier contenant 970 mots aléatoires et uniques en anglais. 
    f = open(nom_fichier, "w")
    for i in range(950) : #On a choisi 950 mots car Faker ne peut pas générer plus de 970 mots et il y a parfois des erreurs lorsque que l'on génère 970 mots.
        mot = fake.unique.word()+"\n"
        mot = mot.upper()
        f.write(mot)
    f.close()

def tri_liste(nom_fichier) :
    f = open(nom_fichier, "r")
    liste = f.readlines()
    liste.sort()
    f.close()
    
    f = open(nom_fichier, "w")
    for i in range(len(liste)) :
        f.write(liste[i])
    f.close

def importer_mots(nomFichier):
    liste=[]
    contenu=[]
    vrai_liste=[]
    with open(nomFichier)as f:
        liste=f.readlines()
        
    for e in liste:
            contenu.append(e.replace(' ','').replace('\n','').replace('ç','c').replace('-','').replace('é','e').replace('è','e').replace('ê','e').replace('à','a').upper())

    for e in contenu:
        if len(e)>=3:
            vrai_liste.append(e) #si le mot contient trois caracteres ou plus, on l'ajoute dans la liste
            
    return vrai_liste

def choisir_mot_alea(liste):
    l=len(liste)
    m=random.randint(0, l-1) #on choisit un indice alea que l'on affecte à la variable m
    mot=liste[m] #cet indice nous permet de récuperer le mot se trouvant en position m dans la liste
    return mot

def initialiser_mot_part_decouv(mot,car_subst='-'):
    l=len(mot)
    i=1
    secret=[]
    start=mot[0] #premiere lettre du mot
    end=mot[l-1] #derniere lettre du mot
    secret.append(start)
    
    while i <= l-2:
        secret.append(car_subst) #les lettres entre la premiere et la derniere sont remplacées par le caractère de substitution
        i+=1
        
    secret.append(end)

    return secret

def afficher_potence_texte(nb_err, nb_err_max):
    if nb_err_max==5:
        potence='-'*5
    else:
        potence='-'*nb_err_max+'!'
        

    if nb_err_max == 5: #cas classique
        if nb_err == 0:
            potence = "-"*5 + '!'
        elif nb_err == 1:
            potence='P'+'-'*(nb_err_max-1)+'!'
        elif nb_err == 2:
            potence='P'+'E'+'-'*(nb_err_max-2)+'!'
        elif nb_err == 3:
            potence= 'P'+'E'+'R'+ '-'*(nb_err_max-3)+'!' #lettres ajoutées en fonction du nombre d'erreurs, ici 3
        elif nb_err == 4:
            potence='P'+'E'+'R'+'D'+ '-'*(nb_err_max-4)+'!'
        elif nb_err == 5:
            potence = 'P'+'E'+'R'+'D'+'U'+'!'
        print(potence)
    else: #cas nombre d'erreurs plus grand
        if nb_err == 1:
            potence='P'+'-'*(nb_err_max-1)+'!'
        elif nb_err == 2:
            potence='P'+'E'+'-'*(nb_err_max-2)+'!'
        elif nb_err == 3:
            potence= 'P'+'E'+'R'+ '-'*(nb_err_max-3)+'!'
        elif nb_err == 4:
            potence='P'+'E'+'R'+'D'+ '-'*(nb_err_max-4)+'!'
        elif nb_err == 5:
            potence = 'P'+'E'+'R'+'D'+'U'+'-'*((nb_err_max)-nb_err)+'!'
        elif nb_err > 5 and nb_err <= nb_err_max-1:
            potence='P'+'E'+'R'+'D'+'U' + '!'*(nb_err-5)+'-'*((nb_err_max)-nb_err)+'!'
        
        print(potence)

def demander_proposition(deja_dit):
    acc=['é','è','à','ê','â','î','ô']
    
    lettre=str(input("\nVeuillez saisir une lettre : "))
    
    if (lettre <"a" and lettre >"z") and (lettre<"A" and lettre>"Z") or lettre in deja_dit :
        while (lettre<"a" and lettre >"z")and(lettre<"A" and lettre>"Z")or lettre in deja_dit: #tant que la saisie n'est pas correct on continue
            lettre=input("\nVeuillez saisir une LETTRE non dite :")
    

    elif len(lettre)>1 or lettre in acc: #on verifie qu"une seule lettre a ete saisie et qu'elle n'est pas accentuée
        while len(lettre)>1 or lettre in acc or (lettre <"a" and lettre >"z") and (lettre<"A" and lettre>"Z") or lettre in deja_dit:
            lettre=input("\nSaisissez une seule LETTRE non accentuée : ")
    
    
    if (lettre>="a" and lettre <="z"):
        return lettre.upper()      
    else:
        return lettre
                       
     


def decouvrir_lettre(lettre,mot_myst,lmot_decouv):
    l=[]
    i=1
    mot_liste=list(mot_myst)
    n=len(mot_liste)
    
    mot_liste.pop(n-1) #on retire le dernier et le premier element afin de comparer dans le bon intervalle
    mot_liste.pop(0)
    
    if lettre not in mot_liste:
        return False
    
    else:
        for e in mot_myst:
            l.append(e)
            
        while i < len(l)-1:
            if lettre == l[i]:
                lmot_decouv[i]=lettre
            
            i+=1
        
        #print(lmot_decouv) #test completion
        return True



def partie_humain(mot_myst,nb_err_max,car_subst='-',mode_graphique=False):
    deja_dit=[]
    nb_err=0
    l=[]
    
    mot_pdecouv=initialiser_mot_part_decouv(mot_myst,car_subst)

    if mode_graphique :
        turtle.write("_ "*len(mot_myst), font=(50))

        mot_en_cours = ["_"]*len(mot_myst)
        deja_dit = []


        while (nb_err < nb_err_max) and ("_" in mot_en_cours) :
            lettre = turtle.textinput("Lettre", "Veuillez saisir une lettre")

            if lettre in deja_dit :
                turtle.penup()
                turtle.goto(0, 150)
                turtle.pendown()
                turtle.write("Lettre déjà utilisé, veuillez en choisir une nouvelle.")
                
            elif lettre in mot_myst:
                turtle.penup()
                turtle.goto(0, 100)
                turtle.pendown()
                for i in range(len(mot_myst)):
                    if lettre == mot_myst[i]:
                        position = i
                        mot_en_cours[position] = lettre
                        turtle.clear()
                        turtle.write(" ".join(mot_en_cours), font=(50))
                
            else:
                nb_err += 1
                turtle.clear()
                turtle.penup()
                turtle.goto(0, 100)
                turtle.pendown()
                turtle.write("  ".join(mot_en_cours), font=(50))
                turtle.penup()
                turtle.goto(0, -100)
                turtle.pendown()
                turtle.write("Nombre d'erreurs : " + str(nb_err) + " / " + str(nb_err_max), font=(20))

            if nb_err < nb_err_max:
                turtle.penup()
                turtle.goto(0, -100)
                turtle.pendown()
                turtle.write("Nombre d'erreurs : " + str(nb_err) + " / " + str(nb_err_max), font=(20))

            deja_dit.append(lettre)

        turtle.penup()
        turtle.goto(0, -300)
        turtle.pendown()

        if "_" not in mot_en_cours:
            turtle.write("Bravo, vous avez gagné !", font=(30))
        else:
            turtle.write("Vous avez perdu. \n\nLe mot était : " + mot_myst, font=(20))

    else :

        potence=afficher_potence_texte(nb_err, nb_err_max)
        print(deja_dit)
        print(mot_pdecouv)
        
        mot_en_liste=list(mot_myst)
        
        lettre=demander_proposition(deja_dit)
        affich=decouvrir_lettre(lettre,mot_myst,mot_pdecouv)
        deja_dit.append(lettre)
        print(affich)
        
        while mot_pdecouv != mot_en_liste and nb_err < nb_err_max:
            if len(deja_dit) >= 1:
                print(deja_dit)
        
            lettre=demander_proposition(deja_dit)
            
            if lettre in mot_myst:
                print("\nLettre présente\n")
                affich=decouvrir_lettre(lettre,mot_myst,mot_pdecouv)
                
            else:
                nb_err+=1
                print("\nNombre d'erreurs : ",nb_err)
                potence=afficher_potence_texte(nb_err, nb_err_max)
                
            
            deja_dit.append(lettre)
            
        
        if nb_err == nb_err_max:
            if nb_err_max == 5:
                print("\nPERDU")
            else:
                la_potence='PERDU'+'!'*(nb_err_max-4)
                print(la_potence)
            print("\nVous avez perdu")
            print("\nLe mot à deviner était : ",mot_myst)
            return False
            
        else:
            la_potence=afficher_potence_texte(nb_err,nb_err_max)
            print("\nVous avez gagné")
            return True

def partie_humain_alea(nom_fichier,nb_err_max,car_subst="-")   :
    listemots=importer_mots(nom_fichier)
    mot_aleatoire=choisir_mot_alea(listemots)
    mot_secret=initialiser_mot_part_decouv(mot_aleatoire)
    partie=partie_humain(mot_aleatoire,nb_err_max)
    
    return partie   

def partie_auto(mot_myst, liste_lettres, affichage=True, pause=False):

    lettres_trouvees = []               # Liste répertoriant les lettres trouvées
    nb_erreurs = 0                      # Nombre d'erreurs

    for lettre in liste_lettres:
        if affichage:
            print("\nLettre proposée : ",lettre)
            if pause:
                input("\nAppuyez sur n'importe quelle touche pour continuer")

        if lettre in mot_myst:
            lettres_trouvees.append(lettre)
        else:
            nb_erreurs += 1
        
        if len(lettres_trouvees) == len(mot_myst): # Quand le nombre de lettres trouvées est égale à la taille du mot, fin de partie
            return nb_erreurs

    return nb_erreurs

#################### Strategie ########################
        
def fabrique_liste_alphabet():
    liste_lettres=[]
    
    for i in range(65,91,1):
        liste_lettres.append(chr(i)) #conversion int en chr code ascii
    
    
    return liste_lettres


def fabrique_liste_alea():
    liste_alea = fabrique_liste_alphabet() # Créer une liste des lettres de l'alphabet

    random.shuffle(liste_alea) # Utilisation de la fonction 'shuffle()' du module 'random' pour mélanger la liste aléatoirement

    return liste_alea


def dico_frequence(nom_fichier):
    dico={}
    liste=[]
    
    with open(nom_fichier)as f:
        liste=f.readlines()
        
    for mot in liste:
        
        for e in mot:
            if e not in dico:
                dico[e]=1
            else:
                dico[e]+=1
        
    del dico['\n']
    
    return dico


def lettre_la_plus_frequente(dico):
    max_lettre=max(dico,key=dico.get)
    
    return max_lettre


def fabrique_liste_freq(nomFichier):
    dico={}
    liste_de_lettres=[]
    dico=dico_frequence(nomFichier)
    max_lettre=lettre_la_plus_frequente(dico)

    tri_dico=dict(sorted(dico.items(),key=lambda item:item[1],reverse=True))# on trie les couples cle/valeur par valeur decroissante
    liste_de_lettres.append(max_lettre)
    del tri_dico[max_lettre]
    for cle in tri_dico:
        liste_de_lettres.append(cle)
        
    return liste_de_lettres

def menu_jeu():
    print("\nJEU DU PENDU : \n")
    choix=input("a. Partie humain normal\nb. Partie humain aleatoire\nc. Partie ordinateur\nq. Quitter\n\nRéponse : ")

    while choix !="a" and choix !="b" and choix !="c" and choix !="q":
        choix=input("Choix incorrect, recommencez : ")

    while choix != "q":
        if choix == "a":
            choix_mot_alea=input("\nVoulez-vous un mot aleatoire ? (o/n) ")
            if choix_mot_alea=="o":
                langue=input("\nMot anglais ou français ? (a/f) ")
                if langue == "a":
                    liste_mots=importer_mots("liste_mots_faker.txt")
                    mot_myst=choisir_mot_alea(liste_mots)
                else:
                    liste_mots=importer_mots("mots.txt")
                    mot_myst=choisir_mot_alea(liste_mots)
            else:
                mot_myst=input("\nEntrez un mot : ")
                
            nb_err_max=int(input("\nNombre d'erreurs max : "))
            reponse = str(input("\nVoulez-vous utiliser le mode graphique (o/n) : "))
            while reponse != "o" and reponse != "n" :
                reponse = str(input("\nRéponse incorrect, recommencez : "))
            if reponse == "o" :
                partieHumain=partie_humain(mot_myst,nb_err_max,car_subst="-", mode_graphique=True)
            else :    
                car_subst=input("\nCaractère de substitution ( '-' par defaut): ")
                partieHumain=partie_humain(mot_myst,nb_err_max,car_subst)
            
        
        if choix == "b":
            nb_err_max=int(input("\nNombre d'erreurs max : "))
            langueFichier=input("\nMots du fichier anglais ou français ? (a/f)")
            if langueFichier=="a":
                partieHumain_alea=partie_humain_alea("liste_mots_faker",nb_err_max)
            else:
                partieHumain_alea=partie_humain_alea("mots.txt",nb_err_max)

        if choix == "c":
            choix_mot_alea=input("\nVoulez vous faire deviner un mot aleatoire ? (o/n) ") #on demande si le joueur veut un mot aleatoire
            if choix_mot_alea == "o":
                langue=input("mot anglais ou français ? (a/f) ") #le fichier generé par faker est un anglais
                if langue == "a": #ajout option langue
                    liste_mots=importer_mots("liste_mots_faker.txt")#mots anglais
                    mot_a_deviner_alea=choisir_mot_alea(liste_mots)
                else:
                    liste_mots=importer_mots("mots.txt")#mots français
                    mot_a_deviner_alea=choisir_mot_alea(liste_mots)
            else:
                mot_a_deviner=input("\nMot à faire deviner : ") #le joueur saisi son mot

            strat=input("\n1. Alphabetique\n2. Aléatoire\n3. Apprentissage\n\nRéponse : ")
            while (strat != "1") and (strat != "2") and (strat != "3") :
                strat = input("\nRéponse incorrect, recommencez : ")

            reponse_pause = input("\nVoulez-vous activer les pauses (o/n) : ")
            while (reponse_pause != "o") and (reponse_pause != "n") :
                reponse_pause = input("\nRéponse incorrect, recommencez : ")
            reponse_affichage = input("\nVoulez-vous activer l'affichage (o/n) : ")
            while (reponse_affichage != "o") and (reponse_affichage != "n")  :
                reponse_affichage = input("\nRéponse incorrect, recommencez : ")

            if (reponse_pause == "o") and (reponse_affichage == "o") :
                
                if strat == "1" :
                    if choix_mot_alea=="o":
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner_alea,liste_de_lettres, affichage= True, pause= True)
                    else:
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner,liste_de_lettres, affichage= True, pause= True)
                    print("\nNombre d'erreurs : ", auto1)
                    
                elif strat == "2" :
                    if choix_mot_alea=="o":
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner_alea,liste_de_lettres_alea, affichage= True, pause= True)
                    else:
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner,liste_de_lettres_alea, affichage= True, pause= True)
                    print("\nNombre d'erreurs : ", auto2)
                    
                elif strat == "3" :
                    if choix_mot_alea=="o":
                        if langue=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq, affichage= True, pause= True)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq, affichage= True, pause= True)
                    else:
                        langue_mot=input("est ce un mot anglais ou français ? (a/f) ")
                        if langue_mot=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq, affichage= True, pause= True)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq, affichage= True, pause= True)
                    print("\nNombre d'erreurs : ", auto3)

            elif (reponse_pause == "n") and (reponse_affichage == "n") :

                if strat == "1" :
                    if choix_mot_alea=="o":
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner_alea,liste_de_lettres, affichage= False)
                    else:
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner,liste_de_lettres, affichage= False)
                    print("\nNombre d'erreurs : ", auto1)
                    
                elif strat == "2" :
                    if choix_mot_alea=="o":
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner_alea,liste_de_lettres_alea, affichage= False)
                    else:
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner,liste_de_lettres_alea, affichage= False)
                    print("\nNombre d'erreurs : ", auto2)
                    
                elif strat == "3" :
                    if choix_mot_alea=="o":
                        if langue=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq, affichage= False)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq, affichage= False)
                    else:
                        langue_mot=input("est ce un mot anglais ou français ? (a/f) ")
                        if langue_mot=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq, affichage= False)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq, affichage= False)
                    print("\nNombre d'erreurs : ", auto3)

            elif (reponse_pause == "o") and (reponse_affichage == "n") :

                if strat == "1" :
                    if choix_mot_alea=="o":
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner_alea,liste_de_lettres, affichage= False, pause= True)
                    else:
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner,liste_de_lettres, affichage= False, pause= True)
                    print("\nNombre d'erreurs : ", auto1)
                    
                elif strat == "2" :
                    if choix_mot_alea=="o":
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner_alea,liste_de_lettres_alea, affichage= False, pause= True)
                    else:
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner,liste_de_lettres_alea, affichage= False, pause= True)
                    print("\nNombre d'erreurs : ", auto2)
                    
                elif strat == "3" :
                    if choix_mot_alea=="o":
                        if langue=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq, affichage= False, pause= True)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq, affichage= False, pause= True)
                    else:
                        langue_mot=input("est ce un mot anglais ou français ? (a/f) ")
                        if langue_mot=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq, affichage= False, pause= True)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq, affichage= False, pause= True)
                    print("\nNombre d'erreurs : ", auto3)
            
            else :

                if strat == "1" :
                    if choix_mot_alea=="o":
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner_alea,liste_de_lettres)
                    else:
                        liste_de_lettres=fabrique_liste_alphabet()
                        auto1=partie_auto(mot_a_deviner,liste_de_lettres)
                    print("\nNombre d'erreurs : ", auto1)
                    
                elif strat == "2" :
                    if choix_mot_alea=="o":
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner_alea,liste_de_lettres_alea)
                    else:
                        liste_de_lettres_alea=fabrique_liste_alea()
                        auto2=partie_auto(mot_a_deviner,liste_de_lettres_alea)
                    print("\nNombre d'erreurs : ", auto2)
                    
                elif strat == "3" :
                    if choix_mot_alea=="o":
                        if langue=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner_alea,liste_freq)
                    else:
                        langue_mot=input("est ce un mot anglais ou français ? (a/f) ")
                        if langue_mot=="a":
                            liste_freq=fabrique_liste_freq("liste_mots_faker.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq)
                        else:
                            liste_freq=fabrique_liste_freq("mots.txt")
                            auto3=partie_auto(mot_a_deviner,liste_freq)
                    print("\nNombre d'erreurs : ", auto3)

        print("\nJEU DU PENDU : \n")        
        choix=input("a. Partie humain normal\nb. Partie humain aleatoire\nc. Partie ordinateur\nq. Quitter\n\nRéponse : ")

        while choix !="a" and choix !="b" and choix !="c" and choix !="q":
            choix=input("Choix incorrect, recommencez : ")
    print("\nAu revoir\n")    
############################################################
        
#nb_err_loose=int(input("Nombre d'erreurs autorisées : "))
faker_liste("liste_mots_faker.txt")
tri_liste("liste_mots_faker.txt")
deja_dit=[]
liste_de_lettres=[]
liste_freq=[]
listemots=importer_mots('mots.txt')
mot_alea=choisir_mot_alea(listemots)
mot_secret=initialiser_mot_part_decouv(mot_alea)
liste_de_lettres=fabrique_liste_alphabet()
liste_de_lettres_alea=fabrique_liste_alea()
liste_freq=fabrique_liste_freq('mots.txt')
print(liste_freq)
#HumainPartie=partie_humain('BONJOUR',nb_err_loose)
#partir_alea=partie_humain_alea('mots.txt',nb_err_loose)                           
#auto=partie_auto(mot_alea,liste_de_lettres)
#auto2=partie_auto(mot_alea,liste_de_lettres_alea)
#auto3=partie_auto(mot_alea,liste_freq)
menu=menu_jeu()

#print("strategie 1: ",auto)
#print("strategie 2: ",auto2)
#print("strategie 3: ",auto3)



